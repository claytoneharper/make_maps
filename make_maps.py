#!/usr/bin/env python

import sys
from bs4 import BeautifulSoup

# make sure filename was provided
if len(sys.argv) <= 2:
    print('Please provide csv filename')
    sys.exit(0)

# get filename
filename = sys.argv[1]
print('Processing {}'.format(filename))

# open file
fips_values = {} # {fips_code: list of values}
with open(filename, 'r') as f:

    # iterate over each line in file
    for line in f:

        # split line into list
        line_split = line.split(',')

        # fips code should be first entry
        fips_code = int(line_split[0])

        # extract remaining values and put them in their own list
        values = []
        for val in line_split[1:]:
            values.append(int(val.strip()) if val != '' else 0)

        # link fips code and those values
        fips_values[fips_code] = values

# load svg file
svg = open('usa_original.svg').read()

# Load into Beautiful Soup
soup = BeautifulSoup(svg, selfClosingTags=['defs', 'sodipodi:namedview'])

# Find counties
paths = soup.findAll('path')

# Map colors
colors = ["#F1EEF6", "#D4B9DA", "#C994C7", "#DF65B0", "#DD1C77", "#980043"]

# County style
path_style = """font-size:12px;
fill-rule:nonzero;
stroke:#FFFFFF;
stroke-opacity:1;
stroke - width: 0.1;
stroke - miterlimit: 4;
stroke - dasharray: none;
stroke - linecap: butt;
marker - start: none;
stroke - linejoin: bevel;
fill:"""

# color each path
for p in paths:

    # extract path id
    fips_code = p['id']

    # skip these ids
    if fips_code in ["State_Lines", "separator"]:
        continue

    # try converting these to an int
    try:
        fips_code = float(fips_code)
    except:
        print('Error: Could not convert fips_code: {}'.format(fips_code))
        continue

    # get values associated with this fips code
    values = fips_values.get(fips_code)
    if not values: # could not find match
        print('Could not find values for fips: {}'.format(fips_code))

        # print out a map for each column
        col_count = 0
        for value in values:
            col_count += 1
            if value > 4:
                color_class = 4
            elif value > 3:
                color_class = 3
            elif value > 2:
                color_class = 2
            elif value > 1:
                color_class = 1
            elif value > 0:
                color_class = 0

            color = colors[color_class]
            p['style'] = path_style + color

    filename = 'output_col{}.svg'.format(col_count)
    print('Creating file: {}'.format(filename))
    open(filename, 'w').write(soup.prettify())

